var soap = require("soap");
var async = require('async');
var mdbClient = require('mongodb').MongoClient;

var url = 'http://infovalutar.ro/curs.asmx?wsdl ';

var date = new Date();
var week = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
var monthWords = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
var monthForDB = monthWords[date.getMonth()];
var year = date.getFullYear();
var month = date.getMonth() + 1;
var currentDate = date.getDate();
var dayOfWeek = week[date.getDay()];

var checkDB = {};

if(dayOfWeek === "Sunday"){
	currentDate = currentDate - 2;
	var getAllArgs = {dt: year + "-" + month + "-" + currentDate + "T00:00:00"};
} else if(dayOfWeek === "Saturday"){
	currentDate = currentDate - 1;
	var getAllArgs = {dt: year + "-" + month + "-" + currentDate + "T00:00:00"};
} else {
	var getAllArgs = {dt: year + "-" + month + "-" + currentDate + "T00:00:00"};
}

checkDB.getLastDateUpdateDB = function(callback){
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('valutarDateUpdate');

		collection.find().toArray(function(err, getLastDateUpdateDB) {
			var getLastDateUpdateDB = getLastDateUpdateDB;
			callback(null, getLastDateUpdateDB);
			db.close();
		})
	})
};

checkDB.getLastDateUpdateService = function(callback){
	soap.createClient(url, function(err, client){
		client.LastDateInserted(function(err, result){
			var result = result;
			callback(null, result);
		})
	});
};

checkDB.createCollections = function(callback){
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var tempDate = {
			updateDate: "0"
		}
		
		db.collection('valutarDateUpdate').insertOne(tempDate,function(err, createCollections){
		})

		db.createCollection("valutar", function(err, createCollections){
		    if (err) throw err;
		   	callback(null, createCollections);
		});
	})
};

async.parallel(checkDB, function(err, checkDB){
	var dateDB = checkDB.getLastDateUpdateDB[0].updateDate;
	var dateService = checkDB.getLastDateUpdateService.LastDateInsertedResult;
	var dbCollections = checkDB.checkCollectionNames;

	if(dateDB != dateService){
		soap.createClient(url, function(err, client){
			mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
				client.getall(getAllArgs, function(err, result){
					var allResults = (result.getallResult.diffgram.DocumentElement);

					var updateDate = {
						updateDate: dayOfWeek + " " +  monthForDB + " " + currentDate + " " + year + " 02:00:00 GMT+0200 (FLE Standard Time)"
					}

					db.collection('valutarDateUpdate').remove({});
					db.collection('valutar').remove({});

					db.collection('valutarDateUpdate').insertOne(updateDate,function(err, result){
					})

					for(var key in allResults){
						var promenl = allResults[key];

						for(var value in promenl){			
							var valuteValues = {
								valute : promenl[value].IDMoneda,
								value : promenl[value].Value
							}

							db.collection('valutar').insertOne(valuteValues,function(err, result){
							})
						}
					}
				})
			});
		})
	}
});
