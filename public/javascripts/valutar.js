function makeCall(){
	var currentURLFull	= $(location).attr('href'); 
	var lastSegment = currentURLFull.split('/').pop();
	var forSend = "http://localhost/" + lastSegment + "/webService";
	$.ajax({
	    type: "GET",
	    url: forSend,
	    dataType: 'json',
	    success: function (data) {
	    	var options = $("#valutar");
	    	for (var i = 0; i < data.valutarContent.length; i++) {
			    options.append($("<option />").val(data.valutarContent[i].value).text(data.valutarContent[i].valute));
	    	}

	    	var leuPrice = '';
	    	for (var i = 0; i < data.priceInLeu.length; i++) {
			    leuPrice = data.priceInLeu[i].value;
	    	}

	    	var defaultPrice = '';
	    	for (var i = 0; i < data.pdpContent.length; i++) {
			    defaultPrice = data.pdpContent[i].price;
	    	}

	    	$("#valutar").change(function(){
		        var selectedValute = $("#valutar").val();
		        var result = (defaultPrice * leuPrice) / selectedValute;
		        document.getElementById("resultedPrice").innerHTML = parseFloat(result).toFixed(2);
		    });
	    },
	    error: function (XMLHttpRequest, textStatus, errorThrown) {
	      console.log(XMLHttpRequest);
	      console.log('error', errorThrown);
	    }
	});
}
