exports.index = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('categories');
		
		collection.find().toArray(function(err, categories) {
			res.render("index", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Project title - welcome",
				categories : categories
			});
			db.close();
		});
	});
};

exports.category = 	function(req, res, next){
	var id = req.params.id;
	var locals = {};
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var async = require('async');

	locals.menuItems = function(callback){
		mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var collection = db.collection('categories');

			collection.find().toArray(function(err, menuItems) {
				var menuItems = menuItems;
				callback(null, menuItems);
				db.close();
			})
		})
	};

	locals.categoryContent = function(callback){
		mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var collection = db.collection('categories');

			collection.find({id: req.params.id}).toArray(function(err, categoryContent) {
				var categoryContent = categoryContent;
				callback(null, categoryContent);
				db.close();
			})
		})
	};

	async.parallel(locals, function(err, locals){
		res.render("category", { 
			// Underscore.js lib
			_     : _, 
			
			// Template data
			menu: locals.menuItems,
			categories : locals.categoryContent
		});
	});
};

exports.subcategory = 	function(req, res, next){
	var id = req.params.id;
	var locals = {};
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var async = require('async');

	locals.menuItems = function(callback){
		mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var collection = db.collection('categories');

			collection.find().toArray(function(err, menuItems) {
				var menuItems = menuItems;
				callback(null, menuItems);
				db.close();
			})
		})
	};

	locals.categoryContent = function(callback){
		mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var collection = db.collection('categories');

			collection.find({id: req.params.categoryid}).toArray(function(err, categoryContent) {
				var categoryContent = categoryContent;
				callback(null, categoryContent);
				db.close();
			})	
		})
	};

	locals.subCategoryContent = function(callback){
		mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var collection = db.collection('products');

			collection.find({ primary_category_id: req.params.subcategoryid }).toArray(function(err, subCategoryContent) {
				var subCategoryContent = subCategoryContent;
				callback(null, subCategoryContent);
				db.close();
			})	
		})
	};

	async.parallel(locals, function(err, locals){
		res.render("subcategory", { 
			// Underscore.js lib
			_     : _, 
			
			// Template data
			cat: req.params.categoryid,
			subprod: req.params.subcategoryid,

			menu: locals.menuItems,
			categories : locals.categoryContent,
			subcategory: locals.subCategoryContent
		});
	});
};

exports.pdp = 	function(req, res, next){
	var id = req.params.id;
	var locals = {};
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var async = require('async');

	locals.menuItems = function(callback){
		mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var collection = db.collection('categories');

			collection.find().toArray(function(err, menuItems) {
				var menuItems = menuItems;
				callback(null, menuItems);
				db.close();
			})
		})
	};

	locals.categoryContent = function(callback){
		mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var collection = db.collection('categories');

			collection.find({id: req.params.categoryid}).toArray(function(err, categoryContent) {
				var categoryContent = categoryContent;
				callback(null, categoryContent);
				db.close();
			})	
		})
	};

	locals.subCategoryContent = function(callback){
		mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var collection = db.collection('products');

			collection.find({ primary_category_id: req.params.subcategoryid }).toArray(function(err, subCategoryContent) {
				var subCategoryContent = subCategoryContent;
				callback(null, subCategoryContent);
				db.close();
			})	
		})
	};

	locals.pdpContent = function(callback){
		mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var collection = db.collection('products');

			collection.find({ id: req.params.productid }).toArray(function(err, pdpContent) {
				var pdpContent = pdpContent;
				callback(null, pdpContent);
				db.close();
			})	
		})
	};

	async.parallel(locals, function(err, locals){
		res.render("pdp", { 
			// Underscore.js lib
			_     : _, 
			
			// Template data
			cat: req.params.categoryid,
			subprod: req.params.subcategoryid,
			productId: req.params.productid,

			menu: locals.menuItems,
			categories : locals.categoryContent,
			subcategory : locals.subCategoryContent,
			pdp : locals.pdpContent,
		});
	});
};

exports.webService = 	function(req, res, next){
	var locals = {};
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var async = require('async');

	locals.valutarContent = function(callback){
		mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var collection = db.collection('valutar');

			collection.find().toArray(function(err, valutarContent) {
				var valutarContent = valutarContent;
				callback(null, valutarContent);
				db.close();
			})
		})
	};

	locals.priceInLeu = function(callback){
		mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var collection = db.collection('valutar');

			collection.find({ valute: "USD"}).toArray(function(err, priceInLeu) {
				var priceInLeu = priceInLeu;
				callback(null, priceInLeu);
				db.close();
			})
		})
	};

	locals.pdpContent = function(callback){
		mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
			var collection = db.collection('products');
			

			collection.find({ id: req.params.productid }).toArray(function(err, pdpContent) {
				var pdpContent = pdpContent;
				callback(null, pdpContent);
				db.close();
			})	
		})
	};

	async.parallel(locals, function(err, locals){
		res.send(locals);

	});
};